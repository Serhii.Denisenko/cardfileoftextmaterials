﻿using AutoMapper;
using CardFileOfTextMaterials.BLL.DTO.In;
using CardFileOfTextMaterials.BLL.Interfaces;
using CardFileOfTextMaterials.DAL.EF;
using CardFileOfTextMaterials.DAL.Entityes;
using CardFileOfTextMaterials.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CardFileOfTextMaterials.BLL.Services
{
    public class CriteriaService : BaseService, ICriteriaService
    {
        public CriteriaService(IMapper mapper, IUnitOfWork<TextMaterialDbContext> unitOfWork)
          : base(mapper, unitOfWork) { }

        public async Task<IEnumerable<CriteriaDTO>> GetAllCriteriaAsync()
        {
            var ColOfCriteria =  Task.Run(() => {
                return _unitOfWork.GetRepository<Criteria>().GetAll();
            }).Result;           

            if (ColOfCriteria == null)
            {
                //&& Exeption?
            }

            List<CriteriaDTO> ColOfCriteriaMediaDTO = new List<CriteriaDTO>();

            foreach (var el in ColOfCriteria)
            {
                ColOfCriteriaMediaDTO.Add(_mapper.Map<CriteriaDTO>(el));
            }

            return ColOfCriteriaMediaDTO;
        }

    }
}
