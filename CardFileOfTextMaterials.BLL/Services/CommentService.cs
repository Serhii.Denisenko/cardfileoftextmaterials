﻿using AutoMapper;
using CardFileOfTextMaterials.BLL.DTO.Out;
using CardFileOfTextMaterials.BLL.Interfaces;
using CardFileOfTextMaterials.DAL.EF;
using CardFileOfTextMaterials.DAL.Entityes;
using CardFileOfTextMaterials.DAL.Identity;
using CardFileOfTextMaterials.DAL.Interfaces;
using System;
using System.Threading.Tasks;
using System.Web;

namespace CardFileOfTextMaterials.BLL.Services
{
    public class CommentService : BaseService, ICommentService
    {
        public CommentService(IMapper mapper, IUnitOfWork<TextMaterialDbContext> unitOfWork)
          : base(mapper, unitOfWork) { }

        public async Task  AddComentAsynk(AddComentDTO addComentDTO)
        {            
           var Media = await _unitOfWork.GetRepository<Media>().GetByIdAsync(addComentDTO.MediaId);         

            Media.Comments.Add(new Comment()
            { Text = addComentDTO.Text,
              MediaId = addComentDTO.MediaId,
              User = addComentDTO.User,
              UserId = addComentDTO.UserId,
              Created = DateTime.Now
            });

           await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteComentAsynk(int commentId, string userId)
        {
            var comment = _unitOfWork.GetRepository<Comment>().GetByIdAsync(commentId).Result;
            if (comment.UserId != userId)
            {
                throw new Exception();            
            }            

             await _unitOfWork.GetRepository<Comment>().RemoveAsync(comment);
             await _unitOfWork.SaveChangesAsync();
        }
    }
}
