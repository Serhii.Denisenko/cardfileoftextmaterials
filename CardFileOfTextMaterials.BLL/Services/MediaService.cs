﻿using AutoMapper;
using CardFileOfTextMaterials.BLL.DTO.In;
using CardFileOfTextMaterials.BLL.DTO.Out;
using CardFileOfTextMaterials.BLL.Interfaces;
using CardFileOfTextMaterials.DAL.EF;
using CardFileOfTextMaterials.DAL.Entityes;
using CardFileOfTextMaterials.DAL.Interfaces;
using GleamTech.VideoUltimate;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFileOfTextMaterials.BLL.Services
{
    public class MediaService : BaseService, IMediaService
    {     
        private const int V = 30;

        public MediaService(IMapper mapper, IUnitOfWork<TextMaterialDbContext> unitOfWork)
            : base(mapper, unitOfWork) { }


        public async Task AddMediaAsync(NewMediaDTO mediaDTO)
        {

            #region SaveVideo

            var listCriteria = new List<Criteria>();

            var ColCriteria =  _unitOfWork.GetRepository<Criteria>().GetAll();

            var SaveMedia = new Media()
            {
                Created = DateTime.Now,
                Name = mediaDTO.Name,
                StorageMediaId = mediaDTO.StorageMediaId,
                Description = mediaDTO.Description              
            };

            foreach (var el in mediaDTO.Criteria)
            {
                if (el.Value)
                {
                    listCriteria.Add(ColCriteria.First(x => x.Name == el.Key));
                    listCriteria[listCriteria.Count - 1].Medias.Add(SaveMedia);
                }

            }

            SaveMedia.Criterias = listCriteria;

           
            var uploadFilesDir = System.Web.HttpContext.Current.Server.MapPath("~/Content/Video");
            Directory.CreateDirectory(uploadFilesDir);                    

            var fileSavePath = Path.Combine(uploadFilesDir,
                CreateName(mediaDTO.Video.FileName, mediaDTO.Name));

                     

            mediaDTO.Video.SaveAs(fileSavePath);

            SaveMedia.PathMedia = "~/Content/Video/" + CreateName(mediaDTO.Video.FileName, mediaDTO.Name);

            #endregion

            #region SavePicture            
            var uploadFilesDirImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/ImagesVideo");
            Directory.CreateDirectory(uploadFilesDirImage);
            VideoFrameReader videoFrameReader;

            videoFrameReader = new VideoFrameReader(fileSavePath);
            
            for (int i = 0; i < V; i++)
                if (videoFrameReader.Read()) 
                {                    
                    if (i == 29 || !videoFrameReader.Read())
                    {
                        var frame = videoFrameReader.GetFrame();
                            
                       frame.Save(Path.Combine(uploadFilesDirImage, mediaDTO.Name + ".jpg"), ImageFormat.Jpeg);
                    }
                }

            SaveMedia.PathJpg = "~/Content/ImagesVideo/" + mediaDTO.Name + ".jpg";           

            #endregion
            
            await _unitOfWork.GetRepository<Media>().AddAsync(SaveMedia);
        }


        public async Task<bool> DislikeMediaAsync(int mediaId, string userId)
        {
            var media = _unitOfWork.GetRepository<Media>().GetByIdAsync(mediaId).Result;
            var dislike = _unitOfWork.GetRepository<Dislike>().GetAll().FirstOrDefault(el => el.UserId == userId && el.MediaId == mediaId);
            var like = _unitOfWork.GetRepository<Like>().GetAll().FirstOrDefault(el => el.UserId == userId && el.MediaId == mediaId);


            if (dislike == null)
            {
                media.Dislikes.Add(new Dislike()
                {
                    MediaId = mediaId,
                    UserId = userId
                });
                var task = await _unitOfWork.SaveChangesAsync();
                //ex

                if (like != null)
                {
                    await _unitOfWork.GetRepository<Like>().RemoveAsync(like);
                }

                return true;
            }
            else
            {
                await _unitOfWork.GetRepository<Dislike>().RemoveAsync(dislike);
                //ex

                return false;
            }

        }

        public async Task<IEnumerable<MainMediaDTO>> GetAllMediasAsync()
        {
            var ColOfMedia = Task.Run(() =>
            {
                return _unitOfWork.GetRepository<Media>().GetAll();
            });

            if (ColOfMedia == null)
            {
                throw new Exception();
            }

            return ColOfMedia.Result.Select(el => _mapper.Map<MainMediaDTO>(el));

        }

        public IEnumerable<MainMediaDTO> GetAllMediasForStorage(int StorageId)
        {

            return _unitOfWork.GetRepository<Media>().GetAll().ToList()
                .Where(el => el.StorageMediaId == StorageId).Select(el => _mapper.Map<MainMediaDTO>(el));
        }

        public IEnumerable<MainMediaDTO> GetAllMediasSortedAsync(SortedObjDTO sortedObjDTO)
        {
            List<string> TypeCollection = sortedObjDTO.TypeMedia.Where(el => el.Value)
                .Select(el => el.Key).ToList();

            IEnumerable<Media> ColOfMedia;

            if (TypeCollection.Count == 0)
            {
                ColOfMedia = _unitOfWork.GetRepository<Media>().GetAll().ToList();
            }
            else
            {
                ColOfMedia = _unitOfWork.GetRepository<Media>().GetAll().ToList()
                    .Where(el => el.Criterias.Select(elC => elC.Name)
                    .Any(elV => TypeCollection.Any(elTC => elTC.Equals(elV))));
            }

            if (sortedObjDTO.PopularityAndData == PopularityDataSorted.ForDataDecrease)
            {
                var RetCol = ColOfMedia.OrderBy(el => el.Created.Year).
                    ThenBy(el => el.Created.Month).ThenBy(el => el.Created.Day)
                    .ThenBy(el => el.Created.Hour);

                return RetCol.Select(el => _mapper.Map<MainMediaDTO>(el));
            }
            else if (sortedObjDTO.PopularityAndData == PopularityDataSorted.ForDataIncrease)
            {
                var RetCol = ColOfMedia.OrderByDescending(el => el.Created.Year).
                    ThenByDescending(el => el.Created.Month).ThenByDescending(el => el.Created.Day)
                    .ThenByDescending(el => el.Created.Hour);

                return RetCol.Select(el => _mapper.Map<MainMediaDTO>(el));
            }
            else if (sortedObjDTO.PopularityAndData == PopularityDataSorted.ForPopularityIncrease)
            {
                return ColOfMedia.OrderByDescending(el => el.Likes.Count - el.Dislikes.Count).Select(el => _mapper.Map<MainMediaDTO>(el));
            }
            else if (sortedObjDTO.PopularityAndData == PopularityDataSorted.ForPopularityDecrease)
            {
                return ColOfMedia.OrderByDescending(el => el.Dislikes.Count - el.Likes.Count).Select(el => _mapper.Map<MainMediaDTO>(el));
            }           
            else
            {
                return ColOfMedia.Select(el => _mapper.Map<MainMediaDTO>(el));
            }
        }

        public async Task<MediaDTO> GetMediaAsync(int? mediaId, string userId)
        {
            if (mediaId == null)
            {
                throw new NotImplementedException();
            }

            var media = await _unitOfWork.GetRepository<Media>().GetByIdAsync((int)mediaId);
            var mediaDTO = _mapper.Map<MediaDTO>(media);

            if (_unitOfWork.GetRepository<Like>().GetAll()
                .Where(el => el.MediaId == mediaId && el.UserId == userId)
                .FirstOrDefault() != null)
            {
                mediaDTO.IsLiked = 1;
            }
            else if (_unitOfWork.GetRepository<Dislike>().GetAll()
                .Where(el => el.MediaId == mediaId && el.UserId == userId)
                .FirstOrDefault() != null)
            {
                mediaDTO.IsLiked = 2;
            }
            else
            {
                mediaDTO.IsLiked = 3;
            }

            return mediaDTO;
        }


        public async Task<bool> LikeMediaAsync(int mediaId, string userId)
        {
            var media = _unitOfWork.GetRepository<Media>().GetByIdAsync(mediaId).Result;
            var dislike = _unitOfWork.GetRepository<Dislike>().GetAll().FirstOrDefault(el => el.UserId == userId && el.MediaId == mediaId);
            var like = _unitOfWork.GetRepository<Like>().GetAll().FirstOrDefault(el => el.UserId == userId && el.MediaId == mediaId);

            if (like == null)
            {
                media.Likes.Add(new Like()
                {
                    MediaId = mediaId,
                    UserId = userId
                });
                var task = await _unitOfWork.SaveChangesAsync();

                //ex

                if (dislike != null)
                {
                    await _unitOfWork.GetRepository<Dislike>().RemoveAsync(dislike);
                }
                return true;
            }
            else
            {
                await _unitOfWork.GetRepository<Like>().RemoveAsync(like);
                //ex

                return false;
            }


        }

        public async Task RemoveMediaAsync(int mediaId)
        {
            var Media = await _unitOfWork.GetRepository<Media>().GetByIdAsync(mediaId);

            if(Media == null)
            {
                throw new ArgumentException();
            }

            await _unitOfWork.GetRepository<Media>().RemoveAsync(Media);
        }

        public Task UpdateMediaAsync(MediaDTO mediaDTO)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<MediaDTO>> IMediaService.GetMediasAsync(int mediaId)
        {
            throw new NotImplementedException();
        }


        public virtual bool CheckCorrectFormat(string name) 
        {
            if(name == null)
            {
                return false;
            }

            StringBuilder formatB = new StringBuilder();
            foreach (var x in name.Reverse())
            {
                if (x == '.')
                {
                    break;
                }
                formatB.Append(x);

            }
            string format="";
            foreach (var el in formatB.ToString().Reverse())
            {
                format += el;
            }
              
            if (format == "ogm" || format == "wmw" || format == "mpeg" || format == "asx" 
                || format == "mpg" || format == "webm"|| format == "mp4" || format == "avi" || format == "mov"
                || format == "m4v")
            {
                return true;
            }
            else
            {
               return false;
            }

        }


        private string CreateName(string Pathname,string name)
        {
            if (CheckCorrectFormat(Pathname))
            {
                string format = "";
                foreach (var x in Pathname.Reverse())
                {
                    if (x == '.')
                    {
                        break;
                    }
                    format+=x;

                }

                format = "." +  String.Concat(format.Reverse());
                return name + format;
            }

            throw new ArgumentException();
        }




    }
}
