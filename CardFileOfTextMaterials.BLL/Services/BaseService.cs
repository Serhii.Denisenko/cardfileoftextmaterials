﻿using AutoMapper;
using CardFileOfTextMaterials.DAL.EF;
using CardFileOfTextMaterials.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// CardFileOfTextMaterials.BLL.Services
/// </summary>
namespace CardFileOfTextMaterials.BLL.Services
{
    /// <summary>
    ///  abstract class BaseService inherits IDisposable
    /// </summary>
    public abstract class BaseService : IDisposable
    {
       
        protected readonly IUnitOfWork<TextMaterialDbContext> _unitOfWork;
        protected readonly IMapper _mapper;

        public BaseService(IMapper mapper, IUnitOfWork<TextMaterialDbContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }


        private bool _isDisposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }

                _isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
    }
}
