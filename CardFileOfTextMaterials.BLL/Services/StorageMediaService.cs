﻿using AutoMapper;
using CardFileOfTextMaterials.BLL.DTO.In;
using CardFileOfTextMaterials.BLL.DTO.Out;
using CardFileOfTextMaterials.BLL.Interfaces;
using CardFileOfTextMaterials.DAL.EF;
using CardFileOfTextMaterials.DAL.Entityes;
using CardFileOfTextMaterials.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardFileOfTextMaterials.BLL.Services
{
    class StorageMediaService : BaseService, IStorageMediaService
    {
        public StorageMediaService(IMapper mapper, IUnitOfWork<TextMaterialDbContext> unitOfWork)
           : base(mapper, unitOfWork) { }

        public async Task AddStorageAsync(AddStorageMediaDTO storageDTO)
        {
            var Storage = new StorageMedia()
            {
                Created = DateTime.Now,
                Name = storageDTO.Name,
                UserId = storageDTO.UserId,
                Description = storageDTO.Description
            };

           await _unitOfWork.GetRepository<StorageMedia>().AddAsync(Storage);
        }

        public Task<StorageMediaDTO> GetStorageAsync(int albumId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<LinkedStorageMediaDTO>> GetStoragesAsync(string userId)
        { 
            var colection =  Task.Run(()=>{
               return _unitOfWork.GetRepository<StorageMedia>().GetAll();
            }).Result;

            return colection.Select(el => _mapper.Map<LinkedStorageMediaDTO>(el));           
        }

        public Task RemoveStorageAsync(int storageId, string userId)
        {
            throw new NotImplementedException();
        }

        public Task UpdateStorageAsync(StorageMediaDTO storageMediaDTO, string userId)
        {
            throw new NotImplementedException();
        }
    }
}
