﻿using CardFileOfTextMaterials.DAL.Entityes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFileOfTextMaterials.BLL.DTO.Out
{
    public class AddStorageMediaDTO
    {
        public int Id { get; set; }

        [StringLength(60, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter name of video")]
        public string Name { get; set; }

        [StringLength(300, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 15)]
        [Required(ErrorMessage = "Please enter name of video")]
        public string Description { get; set; }

        public string UserId { get; set; }

        public virtual User User { get; set; }

        public DateTime Created { get; set; }        
    }
}
