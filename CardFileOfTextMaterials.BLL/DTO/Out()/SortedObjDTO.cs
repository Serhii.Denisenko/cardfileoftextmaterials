﻿using System.Collections.Generic;

namespace CardFileOfTextMaterials.BLL.DTO.Out
{
    public enum PopularityDataSorted
    {
        Default,
        ForPopularityIncrease,
        ForPopularityDecrease,
        ForDataIncrease,
        ForDataDecrease,
    }

    public class SortedObjDTO
    {
        public Dictionary<string, bool> TypeMedia { get; set; }

        public PopularityDataSorted PopularityAndData { get; set; }        
    }
}
