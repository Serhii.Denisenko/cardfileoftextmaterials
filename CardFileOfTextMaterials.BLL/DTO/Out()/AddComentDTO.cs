﻿using CardFileOfTextMaterials.DAL.Entityes;
using System;
using System.ComponentModel.DataAnnotations;

namespace CardFileOfTextMaterials.BLL.DTO.Out
{
    public class AddComentDTO
    {
        [StringLength(500, ErrorMessage = "{0} length must be less {1}.")]
        [Required(ErrorMessage = "Please enter your text")]
        public string Text { get; set; }

        public int MediaId { get; set; }

        public string UserId { get; set; }      

        public User User { get; set; }

        public DateTime Created { get; set; }
    }
}
