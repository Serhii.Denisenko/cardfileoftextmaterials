﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFileOfTextMaterials.BLL.DTO.Out
{
    public class RemoveDTO
    {
        public int IdComent { get; set; }

        public int IdMedia { get; set; }
    }
}
