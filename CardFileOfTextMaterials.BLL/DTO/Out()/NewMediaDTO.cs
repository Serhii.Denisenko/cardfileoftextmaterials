﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace CardFileOfTextMaterials.BLL.DTO.Out
{
    public class NewMediaDTO
    {
        [StringLength(60, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter name of video")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please select video")]
        public HttpPostedFileBase Video { get; set; }

        public DateTime Created { get; set; }

        [StringLength(300, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 15)]
        [Required(ErrorMessage = "Please enter name of video")]
        public string Description { get; set; }

        public int StorageMediaId { get; set; }

        public Dictionary<string, bool> Criteria { get; set; }

        public NewMediaDTO() 
        {
            Criteria = new Dictionary<string, bool>();
        }

    }
}
