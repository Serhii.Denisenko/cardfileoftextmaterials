﻿namespace CardFileOfTextMaterials.BLL.DTO.In
{
    public class LinkedStorageMediaDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
