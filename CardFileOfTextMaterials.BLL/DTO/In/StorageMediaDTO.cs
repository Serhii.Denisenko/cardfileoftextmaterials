﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CardFileOfTextMaterials.BLL.DTO.In
{
    public class StorageMediaDTO
    {
        public int Id { get; set; }

        [StringLength(60, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter name of video")]
        public string Name { get; set; }

        [StringLength(300, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 15)]
        [Required(ErrorMessage = "Please enter name of video")]
        public string Description { get; set; }

        public DateTime Created { get; set; }

        public virtual List<MediaDTO> Medias { get; set; }
    }
}
