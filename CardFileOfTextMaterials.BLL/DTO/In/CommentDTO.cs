﻿namespace CardFileOfTextMaterials.BLL.DTO.In
{
    public class CommentDTO
    {
        public int Id { get; set; }

        public string Text { get; set; }
        
        public int MediaId { get; set; }
        
        public string UserId { get; set; }

        public string UserName { get; set; }
    }
}
