﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CardFileOfTextMaterials.BLL.DTO.In
{
    public class MediaDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string PathMedia { get; set; }

        public string Author { get; set; }

        public string Created { get; set; }

        public int StorageMediaId { get; set; }

        public string Description { get; set; }

        public long AmountComents { get; set; }

        public int AmountLikes { get; set; }

        public int AmountDislikes { get; set; }

        public int? IsLiked { get; set; }
        
        public int RemoveComentId { get; set; }        

        public virtual List<CommentDTO> Comments { get; set; }    

        [MaxLength(400, ErrorMessage = "Comment must be less than 300 symbols")]

        public string NewComent { get; set; }
    }
}

