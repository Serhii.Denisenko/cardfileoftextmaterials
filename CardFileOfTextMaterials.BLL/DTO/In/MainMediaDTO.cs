﻿using System.ComponentModel.DataAnnotations;

namespace CardFileOfTextMaterials.BLL.DTO.In
{
    public class MainMediaDTO
    {
        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string PathJpg { get; set; }

        public string Created { get; set; }
    }   
}
