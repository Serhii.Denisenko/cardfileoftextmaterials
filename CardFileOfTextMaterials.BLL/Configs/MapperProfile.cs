﻿using System;
using System.Globalization;
using AutoMapper;
using CardFileOfTextMaterials.BLL.DTO.In;
using CardFileOfTextMaterials.BLL.DTO.Out;
using CardFileOfTextMaterials.DAL.Entityes;

namespace CardFileOfTextMaterials.BLL.Configs
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<MediaDTO, Media>().ReverseMap().AfterMap((src, dest) => dest.AmountLikes = src.Likes.Count)
                .AfterMap((src, dest) => dest.AmountDislikes = src.Dislikes.Count)
                .AfterMap((src, dest) => dest.AmountComents = src.Comments.Count)
                .AfterMap((src, dest) => dest.AmountDislikes = src.Dislikes.Count)
                .AfterMap((src, dest) => dest.Author = src.StorageMedia.User.UserName)
                .AfterMap((src, dest) => dest.IsLiked = null)
                .AfterMap((src, dest) => dest.Created =
                src.Created.ToString("MMMM d. yyyy", new CultureInfo("en-EN"))
              );
                        
           
            CreateMap<CriteriaDTO, Criteria>().ReverseMap();            
            CreateMap<StorageMedia, StorageMediaDTO>().ReverseMap();
            CreateMap<LinkedStorageMediaDTO, StorageMedia>().ReverseMap();
            CreateMap<CommentDTO, Comment>().ReverseMap();
              //.AfterMap((src, dest) => dest.UserName = src.User.UserName);
            CreateMap<StorageMedia, AddStorageMediaDTO>().ReverseMap();

            CreateMap<AddComentDTO, Comment>().ReverseMap();

            CreateMap<Media, MainMediaDTO>().BeforeMap((src, dest) => dest.UserName = src.StorageMedia.User.UserName)
                .AfterMap((src, dest) => dest.Created = ConvertDataToMainMedia(src.Created));            
        }

        private string ConvertDataToMainMedia(DateTime dateTime)
        {
           
            if (DateTime.Now.Year - dateTime.Year >= 1)
            {
                return DateTime.Now.Year - dateTime.Year + " years ago";
            }
            else if (DateTime.Now.Month - dateTime.Month >= 1)
            {
                return DateTime.Now.Month - dateTime.Month + " months ago";
            }
            else if (DateTime.Now.Day - dateTime.Day >= 1)
            {                
                return DateTime.Now.Day - dateTime.Day + " days ago";
            }
            else if (DateTime.Now.Hour - dateTime.Hour >= 1)
            {
                return DateTime.Now.Hour - dateTime.Hour + " hours ago";
            }
            else if(DateTime.Now.Minute - dateTime.Minute >= 1)
            {
                return DateTime.Now.Minute - dateTime.Minute + " minutes ago";
            }
            else
            {
                return "less one minute";
            }
                        
        }
    }
}
