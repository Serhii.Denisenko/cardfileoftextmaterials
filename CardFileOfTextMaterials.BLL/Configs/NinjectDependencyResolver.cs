﻿using AutoMapper;
using CardFileOfTextMaterials.BLL.Interfaces;
using CardFileOfTextMaterials.BLL.Services;
using CardFileOfTextMaterials.DAL.EF;
using CardFileOfTextMaterials.DAL.Interfaces;
using CardFileOfTextMaterials.DAL.Repositories;
using Ninject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace CardFileOfTextMaterials.BLL.Configs
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;
        public NinjectDependencyResolver()
        {
            kernel = new StandardKernel();
            kernel.Unbind<ModelValidatorProvider>();
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            kernel.Bind<IUnitOfWork<TextMaterialDbContext>>().To<UnitOfWork<TextMaterialDbContext>>().WithConstructorArgument(new TextMaterialDbContext());

            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile<MapperProfile>(); });
            kernel.Bind<IMapper>().ToConstructor(c => new Mapper(mapperConfiguration)).InSingletonScope();

            kernel.Bind<IMediaService>().To<MediaService>().WithConstructorArgument(mapperConfiguration)
                .WithConstructorArgument(TransferSingleton.GetUnitOfWork());

            kernel.Bind<IStorageMediaService>().To<StorageMediaService>().WithConstructorArgument(mapperConfiguration)
               .WithConstructorArgument(TransferSingleton.GetUnitOfWork());

            kernel.Bind<ICriteriaService>().To<CriteriaService>().WithConstructorArgument(mapperConfiguration)
               .WithConstructorArgument(TransferSingleton.GetUnitOfWork());

            kernel.Bind<ICommentService>().To<CommentService>().WithConstructorArgument(mapperConfiguration)
               .WithConstructorArgument(TransferSingleton.GetUnitOfWork());
        }
        
    }
}
