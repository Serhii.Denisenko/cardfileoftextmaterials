﻿using CardFileOfTextMaterials.BLL.DTO.In;
using System.Collections.Generic;
using System.Threading.Tasks;

/// <summary>
/// CardFileOfTextMaterials.BLL.Interfaces
/// </summary>
namespace CardFileOfTextMaterials.BLL.Interfaces
{
    /// <summary>
    /// ICriteriaService
    /// </summary>
    public interface ICriteriaService
    {
        /// <summary>
        /// Get All Criteria
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<CriteriaDTO>> GetAllCriteriaAsync();
    }
}
