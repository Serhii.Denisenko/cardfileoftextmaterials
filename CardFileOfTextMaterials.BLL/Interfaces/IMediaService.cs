﻿using CardFileOfTextMaterials.BLL.DTO.In;
using CardFileOfTextMaterials.BLL.DTO.Out;
using System.Collections.Generic;
using System.Threading.Tasks;

/// <summary>
/// CardFileOfTextMaterials.BLL.Interfaces
/// </summary>
namespace CardFileOfTextMaterials.BLL.Interfaces
{
    /// <summary>
    /// IMediaService
    /// </summary>
    public interface IMediaService
    {
        Task AddMediaAsync(NewMediaDTO mediaDTO);
        Task<MediaDTO> GetMediaAsync(int? mediaId,string userId);
        Task<IEnumerable<MediaDTO>> GetMediasAsync(int mediaId);
        IEnumerable<MainMediaDTO> GetAllMediasSortedAsync(SortedObjDTO sortedObjDTO);
        Task<IEnumerable<MainMediaDTO>> GetAllMediasAsync();
        IEnumerable<MainMediaDTO> GetAllMediasForStorage(int StorageId);
        Task<bool> LikeMediaAsync(int mediaId, string userId);
        Task<bool> DislikeMediaAsync(int mediaId, string userId);
        Task RemoveMediaAsync(int mediaId);
        Task UpdateMediaAsync(MediaDTO mediaDTO);
        bool CheckCorrectFormat(string name);
    }
}
