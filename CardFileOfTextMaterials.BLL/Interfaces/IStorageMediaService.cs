﻿using CardFileOfTextMaterials.BLL.DTO.In;
using CardFileOfTextMaterials.BLL.DTO.Out;
using System.Collections.Generic;
using System.Threading.Tasks;

/// <summary>
/// CardFileOfTextMaterials.BLL.Interfaces
/// </summary>
namespace CardFileOfTextMaterials.BLL.Interfaces
{
    /// <summary>
    /// IStorageMediaService
    /// </summary>
    public interface IStorageMediaService
    {
        Task AddStorageAsync(AddStorageMediaDTO storageDTO);
        Task<StorageMediaDTO> GetStorageAsync(int albumId);
        Task<IEnumerable<LinkedStorageMediaDTO>> GetStoragesAsync(string userId);
        Task RemoveStorageAsync(int storageId, string userId);
        Task UpdateStorageAsync(StorageMediaDTO storageDTO, string userId);
    }
}
