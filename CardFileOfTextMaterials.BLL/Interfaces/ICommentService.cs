﻿using CardFileOfTextMaterials.BLL.DTO.Out;
using System.Threading.Tasks;

/// <summary>
/// CardFileOfTextMaterials.BLL.Interfaces
/// </summary>
namespace CardFileOfTextMaterials.BLL.Interfaces
{
    /// <summary>
    /// ICommentService 
    /// </summary>
    public interface ICommentService
    {
        /// <summary>
        /// Add Coment
        /// </summary>
        /// <param name="addComentDTO"></param>
        /// <returns></returns>
        Task AddComentAsynk(AddComentDTO addComentDTO);

        /// <summary>
        /// Delete Coment
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task DeleteComentAsynk(int commentId, string userId);
    }
}
