﻿using CardFileOfTextMaterials.DAL.Entityes;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

/// <summary>
/// Interfaces IGenericRepository and IUnitOfWork
/// </summary>
namespace CardFileOfTextMaterials.DAL.Interfaces
{
    /// <summary>
    /// public generic interface IUnitOfWork
    /// implements required methods for managing repositories
    /// </summary>
    /// <typeparam name="TContext"> where TContext : DbContext</typeparam>
    public interface IUnitOfWork<TContext> : IDisposable where TContext : DbContext
    {
        TContext DbContext { get;}

        IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity;

        Task<int> SaveChangesAsync();
    }
}
