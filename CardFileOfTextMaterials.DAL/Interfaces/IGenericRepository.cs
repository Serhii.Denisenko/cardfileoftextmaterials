﻿using CardFileOfTextMaterials.DAL.Entityes;
using System.Collections.Generic;
using System.Threading.Tasks;

/// <summary>
/// Interfaces IGenericRepository and IUnitOfWork
/// </summary>
namespace CardFileOfTextMaterials.DAL.Interfaces
{
    /// <summary>
    /// public generic interface IGenericRepository
    /// implements required methods for work with db
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        Task <TEntity> GetByIdAsync(int id);
        IEnumerable<TEntity> GetAll();        
               
        Task AddAsync(TEntity entity);
        Task AddRangeAsync(IEnumerable<TEntity> entities);

        Task RemoveAsync(TEntity entity);
        Task RemoveRangeAsync(IEnumerable<TEntity> entities);

        Task UpdateAsync(TEntity entity);

        Task SaveChangeAsync();
    }
}
