﻿using CardFileOfTextMaterials.DAL.EF;
using CardFileOfTextMaterials.DAL.Entityes;
using CardFileOfTextMaterials.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// namespace that contains the class repository and UnitOfWork
/// </summary>
namespace CardFileOfTextMaterials.DAL.Repositories
{
    /// <summary>
    /// Generic Repository that contains crud and other methods for work with context
    /// inheritance interface IGenericRepository
    /// </summary>
    /// <typeparam name="T"> T is BaseEntity</typeparam>
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// variable context with type DbContext (protected readonly)
        /// </summary>
        protected readonly DbContext _context;

        /// <summary>
        /// Constructor GenericRepository that takes Context of type DbContext and bind _context
        /// and context
        /// </summary>
        /// <param name="context"></param>
        public GenericRepository(DbContext context)
        {                    
            _context = context;
        }
        /// <summary>
        /// async method AddAsync which work with Entity and _context
        /// Add entity to Bd
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Task</returns>
        public async Task AddAsync(T entity)
        {
            _context.Set<T>().Add(entity);
            await _context.SaveChangesAsync();            
        }
        /// <summary>
        /// async method AddRangeAsync which work with Entitys and _context
        /// Add range entity to Bd
        /// </summary>
        /// <param name="entities"></param>
        /// <returns>Task</returns>
        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            _context.Set<T>().AddRange(entities);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        ///  method GetAll which work with Entitys and _context
        /// Takes all entities out of context
        /// </summary>       
        /// <returns>Task<IEnumerable<T>></returns>
        public IEnumerable<T> GetAll()
        {
            return  _context.Set<T>().ToList();           
        }
        /// <summary>
        /// async method GetByIdAsync which work with Entitys and _context
        /// Get the entity by id
        /// </summary>
        /// <param name="id">type int</param>
        /// <returns>Task<T></returns>
        public async Task<T> GetByIdAsync(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }
        /// <summary>
        /// async method RemoveAsync which work with Entitys and _context
        /// removes entities from the database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Task</returns>
        public async Task RemoveAsync(T entity)
        {
             _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// async method RemoveRangeAsync which work with Entitys and _context
        /// removes range entities from the database
        /// </summary>
        /// <param name="entities"></param>
        /// <returns>Task</returns>
        public async Task RemoveRangeAsync(IEnumerable<T> entities)
        {
            _context.Set<T>().RemoveRange(entities);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// standart async method SaveChangeAsync
        /// method forwarding
        /// </summary>
        /// <returns>Task</returns>
        public async Task SaveChangeAsync()
        {
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// async method UpdateAsync which work with Entitys and _context
        /// Update entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Task</returns>
        public async Task UpdateAsync(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
