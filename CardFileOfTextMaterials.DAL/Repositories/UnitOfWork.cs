﻿using CardFileOfTextMaterials.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.Entity;
using CardFileOfTextMaterials.DAL.Entityes;

/// <summary>
/// namespace that contains the class repository and UnitOfWork
/// </summary>
namespace CardFileOfTextMaterials.DAL.Repositories
{
    /// <summary>
    /// genetic class UnitOfWork which is needed to manage repositories
    /// inheritance interface IUnitOfWork
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public class UnitOfWork<TContext> : IUnitOfWork<TContext> where TContext : DbContext
    {
        /// <summary>
        /// private variable _repositories
        /// that is a dictionary where Key is Type and Vallue is copy of Repository 
        /// </summary>
        private Dictionary<Type, object> _repositories;

        /// <summary>
        /// public field DbContext for 
        /// get; private set;
        /// </summary>
        public TContext DbContext { get; private set; }
        /// <summary>
        /// constructor UnitOfWork that takes Context of type DbContext and bind DbContext       
        /// </summary>
        /// <param name="DbContext"></param>
        public UnitOfWork(TContext DbContext)
        {
            this.DbContext = DbContext;
        }

        /// <summary>
        /// generic method returns the desired repository
        /// if the repository is not created, then it creates it
        /// </summary>
        /// <typeparam name="TEntity">where TEntity : BaseEntity</typeparam>
        /// <returns></returns>
        public IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity
        {
            if (_repositories == null)
            {
                _repositories = new Dictionary<Type, object>();
            }

            var type = typeof(TEntity);

            if (!_repositories.ContainsKey(type))
            {
                _repositories.Add(type, new GenericRepository<TEntity>(DbContext));
            }

            return (IGenericRepository<TEntity>)_repositories[type];
        }

        /// <summary>
        /// standart async method SaveChangeAsync
        /// method forwarding 
        /// </summary>
        /// <returns>Task<int></returns>
        public async Task<int> SaveChangesAsync()
        {
            return await DbContext.SaveChangesAsync();
        }

        /// <summary>
        /// IDisposable
        /// </summary>
        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }
        /// <summary>
        ///  protected virtual to override logic  Dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {               
                // clear repositories
                _repositories?.Clear();

                // dispose the db context
                DbContext.Dispose();                
            }

            _disposed = true;
        }
    }
}
