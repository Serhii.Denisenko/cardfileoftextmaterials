﻿using CardFileOfTextMaterials.DAL.EF;

/// <summary>
///  namespace that contains the class repository and UnitOfWork
/// </summary>
namespace CardFileOfTextMaterials.DAL.Repositories
{
    /// <summary>
    /// class TransferSingleton is needed to guarantee one copy in the program to save memory of RAM
    /// </summary>
    public class TransferSingleton
    {
        /// <summary>
        /// private static variable _unitOfWork 
        /// type is UnitOfWork<TextMaterialDbContext>
        /// </summary>
        private static UnitOfWork<TextMaterialDbContext> _unitOfWork;

        private TransferSingleton(){ }

        /// <summary>
        /// static method GetUnitOfWork that distributes copies UnitOfWork
        /// </summary>
        /// <returns></returns>
        public static UnitOfWork<TextMaterialDbContext> GetUnitOfWork()
        {
            if (_unitOfWork == null)
            {
                _unitOfWork = new UnitOfWork<TextMaterialDbContext>(new TextMaterialDbContext());
            }

            return _unitOfWork;
        }
    }
}
