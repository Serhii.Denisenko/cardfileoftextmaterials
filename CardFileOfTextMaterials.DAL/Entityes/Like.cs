﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// namespace with entityes
/// </summary>
namespace CardFileOfTextMaterials.DAL.Entityes
{
    /// <summary>
    /// 
    /// </summary>
    public class Like : BaseEntity
    {
        public int MediaId { get; set; }
        public virtual Media Media { get; set; }

        public string UserId { get; set; }
        public virtual User User { get; set; }
    }
}
