﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Collections.ObjectModel;

/// <summary>
/// namespace with entityes
/// </summary>
namespace CardFileOfTextMaterials.DAL.Entityes
{
    /// <summary>
    /// 
    /// </summary>
    public class User : IdentityUser
    {
        public override string Id { get; set; }

        public virtual ICollection<StorageMedia> StorageMedia { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Like> Likes { get; set; }

        public virtual ICollection<Dislike>  Dislikes { get; set; } 

        public User() {

            StorageMedia = new Collection<StorageMedia>();
            Comments = new Collection<Comment>();
            Likes = new Collection<Like>();
            Dislikes = new Collection<Dislike>();
        }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }
    }
}
