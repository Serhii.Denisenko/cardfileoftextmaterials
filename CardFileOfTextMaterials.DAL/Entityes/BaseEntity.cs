﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// namespace with entityes
/// </summary>
namespace CardFileOfTextMaterials.DAL.Entityes
{
    /// <summary>
    /// Base class for an entityes that contains an Id  of type int    
    /// </summary>
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
