﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

/// <summary>
/// namespace with entityes
/// </summary>
namespace CardFileOfTextMaterials.DAL.Entityes
{
    /// <summary>
    /// 
    /// </summary>
    public class Media : BaseEntity
    {
        public string Name { get; set; }
        public string PathMedia { get; set; }
        public string PathJpg { get; set; }

        public DateTime Created { get; set; }

        public string Description { get; set; }

        public int StorageMediaId { get; set; }
        public virtual StorageMedia StorageMedia { get; set; }
                
        public virtual ICollection<Criteria> Criterias{ get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Like> Likes { get; set; }
        public virtual ICollection<Dislike> Dislikes { get; set; }

        public Media()
        {
            Criterias = new Collection<Criteria>();
            Comments = new Collection<Comment>();
            Likes = new Collection<Like>();
            Dislikes = new Collection<Dislike>();
        }

    }
}
