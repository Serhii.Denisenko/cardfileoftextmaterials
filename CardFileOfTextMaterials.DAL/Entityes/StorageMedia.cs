﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

/// <summary>
/// namespace with entityes
/// </summary>
namespace CardFileOfTextMaterials.DAL.Entityes
{
    /// <summary>
    /// 
    /// </summary>
    public class StorageMedia : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime Created { get; set; }

        public string UserId { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<Media> Medias{ get; set; }

        public StorageMedia() {

            Medias = new Collection<Media>();
        }
    }   

}

