﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// namespace with entityes
/// </summary>
namespace CardFileOfTextMaterials.DAL.Entityes
{
    /// <summary>
    /// 
    /// </summary>
    public class Criteria : BaseEntity
    {
        public string Name { get; set; }
     
        public virtual ICollection<Media> Medias { get; set; }
    }
}
