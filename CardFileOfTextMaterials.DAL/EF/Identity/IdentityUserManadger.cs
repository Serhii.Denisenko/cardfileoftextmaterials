﻿using CardFileOfTextMaterials.DAL.Entityes;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace CardFileOfTextMaterials.DAL.Identity
{
    class IdentityUserManadger : UserManager<User>
    {
        public IdentityUserManadger(IUserStore<User> store)
           : base(store)
        { }

        public static IdentityUserManadger Create(IdentityFactoryOptions<IdentityUserManadger> options,
           IOwinContext context)
        {
            IdentityDbContext db = context.Get<IdentityDbContext>();
            IdentityUserManadger manager = new IdentityUserManadger(new UserStore<User>(db));
            return manager;
        }
    }
}
