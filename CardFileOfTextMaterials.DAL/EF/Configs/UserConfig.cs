﻿using CardFileOfTextMaterials.DAL.Entityes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Text;

/// <summary>
/// namespace Configs
/// </summary>
namespace CardFileOfTextMaterials.DAL.EF.Configs
{
    public class UserConfig : EntityTypeConfiguration<User>
    {
        public UserConfig()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.UserName).IsRequired();
            this.Property(p => p.UserName).HasMaxLength(200);
            this.HasMany(p => p.StorageMedia)
            .WithRequired(p => p.User)
            .HasForeignKey(p => p.UserId)
            .WillCascadeOnDelete(false);
            this.HasMany(p => p.StorageMedia)
              .WithRequired(p => p.User)
              .HasForeignKey(p => p.UserId)
              .WillCascadeOnDelete(false);
            this.HasMany(p => p.Comments)
              .WithRequired(p => p.User)
              .HasForeignKey(p => p.UserId)
              .WillCascadeOnDelete(false);
            this.HasMany(p => p.Likes)
               .WithRequired(p => p.User)
               .HasForeignKey(p => p.UserId)
               .WillCascadeOnDelete(false);
            this.HasMany(p => p.Dislikes)
               .WithRequired(p => p.User)
               .HasForeignKey(p => p.UserId)
               .WillCascadeOnDelete(false);
        }
    }
}

