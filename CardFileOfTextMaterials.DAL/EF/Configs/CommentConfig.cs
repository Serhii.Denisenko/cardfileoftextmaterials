﻿using CardFileOfTextMaterials.DAL.Entityes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Text;

/// <summary>
/// namespace Configs
/// </summary>
namespace CardFileOfTextMaterials.DAL.EF.Configs
{
    public class CommentConfig : EntityTypeConfiguration<Comment>
    {
        public CommentConfig() 
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.MediaId).IsRequired();
            this.Property(p => p.Text).HasMaxLength(600);
            this.Property(p => p.UserId).IsRequired();
            this.HasRequired(p => p.Media)
          .WithMany(p => p.Comments);
            this.HasRequired(p => p.User)
            .WithMany(p => p.Comments);
        }
    }
}
