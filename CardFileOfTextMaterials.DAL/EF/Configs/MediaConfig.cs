﻿using CardFileOfTextMaterials.DAL.Entityes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Text;

/// <summary>
/// namespace Configs
/// </summary>
namespace CardFileOfTextMaterials.DAL.EF.Configs
{
    public class MediaConfig : EntityTypeConfiguration<Media>
    {
        public MediaConfig()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.StorageMediaId).IsRequired();
            this.Property(p => p.PathMedia).IsRequired();
            this.HasMany(p => p.Likes)
             .WithRequired(p => p.Media)
             .HasForeignKey(p => p.MediaId)
             .WillCascadeOnDelete(true);
            this.HasMany(p => p.Dislikes)
               .WithRequired(p => p.Media)
               .HasForeignKey(p => p.MediaId)
               .WillCascadeOnDelete(true);
            this.HasMany(p => p.Comments)
                .WithRequired(p => p.Media)               
                .HasForeignKey(p => p.MediaId)
                .WillCascadeOnDelete(true);
            this.HasMany(p => p.Criterias)
             .WithMany(p => p.Medias);
        }
    }
}
