﻿using CardFileOfTextMaterials.DAL.Entityes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Text;

/// <summary>
/// namespace Configs
/// </summary>
namespace CardFileOfTextMaterials.DAL.EF.Configs
{
    public class StorageMediaConfig : EntityTypeConfiguration<StorageMedia>
    {
        public StorageMediaConfig()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.UserId).IsRequired();
            this.Property(p => p.Name).HasMaxLength(400);
            this.HasMany(p => p.Medias)
              .WithRequired(p => p.StorageMedia)
              .HasForeignKey(p => p.StorageMediaId)
              .WillCascadeOnDelete(true);
        }
    }
}
