﻿using CardFileOfTextMaterials.DAL.Entityes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Text;

/// <summary>
/// namespace Configs
/// </summary>
namespace CardFileOfTextMaterials.DAL.EF.Configs
{
    public class LikeConfig : EntityTypeConfiguration<Like>
    {
        public LikeConfig()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.MediaId).IsRequired();
            this.Property(p => p.UserId).IsRequired();
        }
    }
}
