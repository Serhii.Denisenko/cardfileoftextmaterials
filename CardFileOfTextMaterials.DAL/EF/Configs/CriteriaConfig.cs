﻿using CardFileOfTextMaterials.DAL.Entityes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Text;

/// <summary>
/// namespace Configs
/// </summary>
namespace CardFileOfTextMaterials.DAL.EF.Configs
{
    public class CriteriaConfig : EntityTypeConfiguration<Criteria>
    {
        public CriteriaConfig()
        {
            this.HasKey(p => p.Id);            
            this.Property(p => p.Name).HasMaxLength(120);            
        }       
    }
}
