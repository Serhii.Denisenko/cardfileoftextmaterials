﻿using CardFileOfTextMaterials.DAL.Entityes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Text;

/// <summary>
/// namespace Configs
/// </summary>
namespace CardFileOfTextMaterials.DAL.EF.Configs
{
    public class DislikeConfig : EntityTypeConfiguration<Dislike>
    {
        public DislikeConfig() 
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.MediaId).IsRequired();
            this.Property(p => p.UserId).IsRequired();           
        }       
    }
}
