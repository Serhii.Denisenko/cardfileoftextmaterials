﻿using CardFileOfTextMaterials.DAL.Entityes;
using CardFileOfTextMaterials.DAL.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using System.Linq;

/// <summary>
/// namespace Entity Framework 
/// </summary>
namespace CardFileOfTextMaterials.DAL.EF
{
    /// <summary>
    /// Initializer that inherits CreateDatabaseIfNotExists<TextMaterialDbContext>
    /// </summary>
    class Initializer : CreateDatabaseIfNotExists<TextMaterialDbContext>
    {
        protected override void Seed(TextMaterialDbContext context)
        {

            #region Role
            var userManager = new ApplicationUserManager(new UserStore<User>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                    

            // создаем две роли
            var role1 = new IdentityRole { Name = "Admin" };
            var role2 = new IdentityRole { Name = "User" };

            // добавляем роли в бд
            roleManager.Create(role1);
            roleManager.Create(role2);

            // создаем пользователей
            var admin = new User { Email = "TestAdmin_@gmail.com", UserName = "TestAdmin" };
            string passwordA = "TestData123_";
            var resultA = userManager.Create(admin, passwordA);

            // если создание пользователя прошло успешно
            if (resultA.Succeeded)
            {
                // добавляем для пользователя роль
                userManager.AddToRole(admin.Id, role1.Name);              
            }
           
            // создаем пользователей
            var user = new User { Email = "TestUser_@gmail.com", UserName = "TestUser" };
            string passwordU = "TestData123_";
            var resultU = userManager.Create(user, passwordU);

            // если создание пользователя прошло успешно
            if (resultU.Succeeded)
            {
                // добавляем для пользователя роль
                userManager.AddToRole(user.Id, role2.Name);
            }
            #endregion

            #region Criteria
            List<Criteria> listCriteria = new List<Criteria>()
            {
                new Criteria(){ Id=1, Name = "Training" },
                new Criteria(){ Id=2, Name = "Popular science" },
                new Criteria(){ Id=3, Name = "Blogs" },
                new Criteria(){ Id=4, Name = "Game" },
                new Criteria(){ Id=5,Name =  "Entertaining" },
                new Criteria(){ Id=6, Name = "Historical" },
                new Criteria(){ Id=7, Name = "Music"}       
            };

            foreach (Criteria item in listCriteria)
                context.criterias.Add(item);
            context.SaveChanges();
            #endregion

            #region AddTestAlbumAndMedia

            var storMedia = new StorageMedia() { Id = 1, Name = "TestStorage", User = user,
                Created = DateTime.Now, Description = "Test 111", UserId = user.Id
            };

            List<Media> medias = new List<Media>()
            {
                new Media { Criterias = new List<Criteria>(){ listCriteria[1] },
                 Name = "TestPopularScience", PathMedia="~/Content/Video/PopScien.mp4"
                , StorageMedia = storMedia, Description = "Nice Video PopScien", StorageMediaId = storMedia.Id, Id = 1, Created= DateTime.Now
                , PathJpg="~/Content/ImagesVideo/PopScience.jpg"},
                new Media { Criterias = new List<Criteria>(){ listCriteria[6] },
                 Name = "TestMusic", PathMedia="~/Content/Video/music.mp4"
                , StorageMedia = storMedia, Description = "Nice Video music", StorageMediaId = storMedia.Id, Id = 2, Created= DateTime.Now
                , PathJpg="~/Content/ImagesVideo/Music.jpg"},
                new Media { Criterias = new List<Criteria>(){ listCriteria[5] },
                 Name = "TestHistorical", PathMedia="~/Content/Video/historical.mp4"
                , StorageMedia = storMedia, Description = "Nice Video historical", StorageMediaId = storMedia.Id, Id = 3, Created= DateTime.Now
                , PathJpg="~/Content/ImagesVideo/HistoricalTest.jpg"},
                new Media { Criterias = new List<Criteria>(){ listCriteria[0]},
                 Name = "TestTrain", PathMedia="~/Content/Video/Train.mp4"
                , StorageMedia = storMedia, Description = "Nice Video Train", StorageMediaId = storMedia.Id, Id = 4, Created= DateTime.Now
                , PathJpg="~/Content/ImagesVideo/Training.jpg"}

            };
            storMedia.Medias = medias;

            context.storageMedias.Add(storMedia);
            context.SaveChanges();

            #endregion

            base.Seed(context);

            context.SaveChanges();
            base.Seed(context);
        }
    }
}
