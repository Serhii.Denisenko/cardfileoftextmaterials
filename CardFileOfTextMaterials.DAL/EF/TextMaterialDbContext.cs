﻿using CardFileOfTextMaterials.DAL.Entityes;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Entity;
using System.Reflection;
using CardFileOfTextMaterials.DAL.EF.Configs;

/// <summary>
/// namespace Entity Framework
/// </summary>
namespace CardFileOfTextMaterials.DAL.EF
{
    /// <summary>
    /// TextMaterialDbContext inherits IdentityDbContext<User>
    /// </summary>
    public class TextMaterialDbContext : IdentityDbContext<User>
    {
        public TextMaterialDbContext() : base("name=DefaultConnection") { }

        static TextMaterialDbContext()
        {            
            Database.SetInitializer<TextMaterialDbContext>(new Initializer());
        }

        public DbSet<StorageMedia> storageMedias { get; set; }

        public DbSet<Comment> comments { get; set; }

        public DbSet<Criteria> criterias { get; set; }

        public DbSet<Like> likes { get; set; }

        public DbSet<Dislike> dislikes { get; set; }

        public DbSet<Media> medias { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Configurations.Add(new CommentConfig());
            builder.Configurations.Add(new CriteriaConfig());
            builder.Configurations.Add(new DislikeConfig());
            builder.Configurations.Add(new LikeConfig());
            builder.Configurations.Add(new MediaConfig());           
            builder.Configurations.Add(new StorageMediaConfig());
            builder.Configurations.Add(new UserConfig());      
                     
            base.OnModelCreating(builder);            
        }

        public static TextMaterialDbContext Create()
        {
            return new TextMaterialDbContext();
        }

    }
}
