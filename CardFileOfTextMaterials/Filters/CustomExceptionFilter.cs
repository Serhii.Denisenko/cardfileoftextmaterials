﻿using System;
using System.Web.Mvc;

namespace CardFileOfTextMaterials.Filters
{
    public class CustomExceptionFilter : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {            
            filterContext.Result = new ContentResult
            {
              Content = "Exeption"
            };

            Console.WriteLine("Exeption");
            filterContext.ExceptionHandled = true;
        }
    }
}