﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CardFileOfTextMaterials.Startup))]
namespace CardFileOfTextMaterials
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
