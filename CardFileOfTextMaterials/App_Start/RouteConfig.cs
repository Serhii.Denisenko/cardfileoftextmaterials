﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CardFileOfTextMaterials
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
              name: "WatchVideo",
              url: "HomeController/WatchMedia/{id}",
              defaults: new { controller = "HomeController", action = "WatchMedia", id = UrlParameter.Optional },
              constraints: new { id = @"\d+" }
          );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional}              
            );


            //routes.MapRoute(name: "Default", url: "{controller}/{action}/{id}/{*catchall}", 
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });

        }
    }
}
