﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CardFileOfTextMaterials.BLL.Interfaces;
using CardFileOfTextMaterials.DAL.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using CardFileOfTextMaterials.BLL.DTO.In;
using CardFileOfTextMaterials.BLL.DTO.Out;
using System;

namespace CardFileOfTextMaterials.Controllers
{
    [Authorize(Roles = "User,Admin")]
    public class UserController : Controller
    {
        private IMediaService _mediaService;
        private ICriteriaService _criteriaService;
        private IStorageMediaService _storageMediaService;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        public UserController(IMediaService mediaService, IStorageMediaService storageMediaService, ICriteriaService criteriaService) 
        {           
            _mediaService = mediaService;
            _storageMediaService = storageMediaService;
            _criteriaService = criteriaService;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public async Task<ActionResult> Index()
        {
            try
            {
                string Id = UserManager.FindByNameAsync(User.Identity.Name).Result.Id;

                var Collection = await _storageMediaService.GetStoragesAsync(Id);

                if (Collection != null)
                {
                    ViewBag.CollectionStorage = Collection.ToList();
                }
                else
                {
                    ViewBag.CollectionStorage = new List<LinkedStorageMediaDTO>();
                }
                ViewBag.Collection = new List<MainMediaDTO>();
                return View();
            }
            catch (Exception ex) 
            {
                // loger
                return View("General");
            }           
        }

        public ActionResult MainCabinet()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddStorage()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddStorage(AddStorageMediaDTO storageMediaDTO)
        {
            try
            {
                var user = UserManager.FindByNameAsync(User.Identity.Name).Result;               
                storageMediaDTO.UserId = user.Id;
                await _storageMediaService.AddStorageAsync(storageMediaDTO);
                return View("Index");
            }
            catch (Exception ex)
            {
                return View("General");
            }
        }

        public ActionResult GetStorage(int? Id)
        {
            try
            {
                ViewBag.Add = true;
                if (Id == null)
                {
                    return View("General");
                }

                string IdUser = UserManager.FindByNameAsync(User.Identity.Name).Result.Id;

                var CollectionStorage = _storageMediaService.GetStoragesAsync(IdUser);

                ViewBag.Id = Id;
                if (CollectionStorage != null)
                {
                    ViewBag.CollectionStorage = CollectionStorage.Result.ToList();
                }
                else
                {
                    ViewBag.CollectionStorage = new List<LinkedStorageMediaDTO>();
                }
                var MediaCollection = _mediaService.GetAllMediasForStorage((int)Id);

                if (MediaCollection != null)
                {
                    ViewBag.Collection = MediaCollection.ToList();
                }
                else
                {
                    ViewBag.Collection = new List<LinkedStorageMediaDTO>();
                }

                return View("index");
            }
            catch (Exception ex)
            {
                return View("General");
            }
        }

        public ActionResult EditMedia(int? Id) 
        {
            return View("General");
            return View();
        }

        public ActionResult WatchMedia(int? Id)
        {
            return View("General");
            return View();
        }

        public ActionResult RemoveMedia(int? Id)
        {
            try 
            {
                if (Id == null)
                {
                    return View("General");
                }
                _mediaService.RemoveMediaAsync((int)Id);
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                // logger
                return View("General");
            }
          
           
        }


        public ActionResult AddVideo(int? Id)
        {
            try
            {
                if (Id == null)
                {
                    return View("General");
                }

                var model = new NewMediaDTO()
                {
                    StorageMediaId = (int)Id
                };

                var collection = _criteriaService.GetAllCriteriaAsync().Result.ToList();

                var listNames = new List<string>();
                foreach (var el in collection)
                {
                    model.Criteria.Add(el.Name, false);
                    listNames.Add(el.Name);
                }

                ViewBag.CollectionCriteria = listNames;
                return View(model);
            }
            catch(Exception ex)
            {
                // logger
                return View("General");
            }       
            
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadVideo(NewMediaDTO newMediaDTO)
        {
            try
            {
                var httpPostedFile = Request.Files[0];

                if (httpPostedFile != null)
                {
                    if (_mediaService.CheckCorrectFormat(newMediaDTO.Video.FileName))
                        _mediaService.AddMediaAsync(newMediaDTO);
                    else
                    {
                        ModelState.AddModelError("file", "Incorect type of video. Please select other format.");
                        return RedirectToAction("UploadVideo", new { newMediaDTO = newMediaDTO });
                    }
                }
                else
                {
                    return View("General");
                }
            }
            catch (Exception ex)
            {
                // logger
                return View("General");
            }

            return RedirectToAction("GetStorage" ,"User", new { Id = newMediaDTO.StorageMediaId });            
        }

    }
}
