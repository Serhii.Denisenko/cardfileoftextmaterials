﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CardFileOfTextMaterials.BLL.DTO.In;
using CardFileOfTextMaterials.BLL.DTO.Out;
using CardFileOfTextMaterials.BLL.Interfaces;
using CardFileOfTextMaterials.DAL.Identity;
using CardFileOfTextMaterials.Filters;
using Microsoft.AspNet.Identity.Owin;

namespace CardFileOfTextMaterials.Controllers
{
    public class HomeController : Controller
    {
        private IMediaService _mediaService;
        private ICriteriaService _criteriaService;
        private ICommentService _commentService;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        public HomeController(IMediaService mediaService, ICriteriaService criteriaService, ICommentService commentService)
        {
            _mediaService = mediaService;
            _criteriaService = criteriaService;
            _commentService = commentService;
        }

        public HomeController()
        {
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
                      

        [HttpGet] 
        public ActionResult Index()
        {
            try
            {
                ViewBag.Collection = _mediaService.GetAllMediasAsync().Result.ToList();

                var collection = _criteriaService.GetAllCriteriaAsync().Result.ToList();
                var listNames = new List<string>();
                foreach (var el in collection)
                {
                    listNames.Add(el.Name);
                }

                ViewBag.CollectionCriteria = listNames;
            }
            catch(Exception ex)
            {
                // loger
                return View("Index");
            }
            return View();
        }
        

        [HttpPost]
        public ActionResult SortedMain(SortedObjDTO sortedObjDTO)
        {
            try
            {
                ViewBag.Collection = _mediaService.GetAllMediasSortedAsync(sortedObjDTO).ToList();

                var collection = _criteriaService.GetAllCriteriaAsync().Result.ToList();
                var listNames = new List<string>();
                foreach (var el in collection)
                {
                    listNames.Add(el.Name);
                }

                ViewBag.CollectionCriteria = listNames;

                return View("Index");
            }
            catch (Exception ex)
            {
                // loger
                return View("General");
            }
        }                
      
        [Authorize]
        public async Task<ActionResult> WatchMedia(int? id)
        {
            try
            {
                if (id == null)
                {
                    return View("General");
                }

                string IdUser = UserManager.FindByNameAsync(User.Identity.Name).Result.Id;
                var media = await _mediaService.GetMediaAsync(id, IdUser);
                ViewBag.IdUser = IdUser;
                return View(media);
            }
            catch (Exception ex)
            {
                // loger
                return View("General");
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Like(MediaDTO mediaDTO)
        {
          
            string IdUser = UserManager.FindByNameAsync(User.Identity.Name).Result.Id;
            var IsLike = await _mediaService.LikeMediaAsync(mediaDTO.Id, IdUser);
            var media = await _mediaService.GetMediaAsync(mediaDTO.Id, IdUser);
            
            if(IsLike)  media.IsLiked = 1;
            else  media.IsLiked = 3;

            ViewBag.IdUser = IdUser;
            return View("WatchMedia", media);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Dislike(MediaDTO mediaDTO)
        {
            try
            {

                string IdUser = UserManager.FindByNameAsync(User.Identity.Name).Result.Id;
                var IsDilike = await _mediaService.DislikeMediaAsync(mediaDTO.Id, IdUser);
                var media = await _mediaService.GetMediaAsync(mediaDTO.Id, IdUser);

                if (IsDilike) media.IsLiked = 2;
                else media.IsLiked = 3;

                ViewBag.IdUser = IdUser;
                return View("WatchMedia", media);
            }
            catch(Exception ex)
            {
                // loger
                return View("General");
            }
        }

        [Authorize]
        [HttpPost]
        [CustomExceptionFilter]
        public async Task<ActionResult> NewComent(MediaDTO mediaDTO)
        {

            throw new Exception();
         
                string IdUser = UserManager.FindByNameAsync(User.Identity.Name).Result.Id; 
                await _commentService.AddComentAsynk(new AddComentDTO()
                {
                    Text = mediaDTO.NewComent,
                    MediaId = mediaDTO.Id,
                    UserId = IdUser
                   // User = UserManager.FindByNameAsync(User.Identity.Name).Result
                });
                var media = await _mediaService.GetMediaAsync(mediaDTO.Id, IdUser);

                ViewBag.IdUser = IdUser;
                return View("WatchMedia", media);
            
            
            
        }


        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RemoveComent(int? IdComent, int? IdMedia)
        {
            try
            {

                if (IdComent == null || IdMedia == null)
                {
                    return View("General");
                }

                string IdUser = UserManager.FindByNameAsync(User.Identity.Name).Result.Id;
                await _commentService.DeleteComentAsynk((int)IdComent, IdUser);
                var media = await _mediaService.GetMediaAsync(IdMedia, IdUser);

                ViewBag.IdUser = IdUser;
                return View("WatchMedia", media);
            }
            catch (Exception ex)
            {
                // loger
                return View("General");
            }
        }


    }
}